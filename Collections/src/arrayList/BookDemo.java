package arrayList;

import java.util.ArrayList;
import java.util.List;

public class BookDemo {
	public static void main(String[] args) {

		List<Book> bookList = new ArrayList<>();
		bookList.add(new Book("secret", "Rhonda Byrne", "Atria"));
		bookList.add(new Book("Moonrise", "Sarah Crossan", "Bloomsbury Publishing"));
		bookList.add(new Book("Harry Potter and the Goblet of Fire", "J.K. Rowling", "Bloomsbury Publishing"));
		
		for (Book b : bookList) {
			if(b.getPublisher().equals("Bloomsbury Publishing"))
				System.out.println(b);
		}
		
	}
}
