package arrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class EmployeeDemo {
	public static void main(String[] args) {

		List<Employee> empList = new ArrayList<>();
		List<Employee> FresherList = new ArrayList<>();
		
		empList.add(new Employee("Shivani", 40000));
		empList.add(new Employee("Alekhya", 32000));
		empList.add(new Employee("Haritha", 18000));
		empList.add(new Employee("Bhavani", 19000));
		empList.sort(null);
		
		Collections.sort(empList);
		
		Iterator<Employee> itr = empList.iterator();
		
		while (itr.hasNext()) {
			Employee e = itr.next();
			if (e.getSalary() < 20000) {
				itr.remove();
				FresherList.add(e);
			}
		}
		System.out.println("Employee List");
		
		for (Employee e : empList) {
			System.out.println(e);
		}
		
		System.out.println("Fresher List");
		
		Iterator<Employee> itr1 = FresherList.iterator();
		while (itr1.hasNext()) {
			System.out.println(itr1.next());
		}

	}
}