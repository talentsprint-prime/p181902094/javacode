package arrayList;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthScrollBarUI;

public class LeftRotation {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		String[] nd = scanner.nextLine().split(" ");

		int n = Integer.parseInt(nd[0]);

		int d = Integer.parseInt(nd[1]);

		int[] a = new int[n];

		String[] aItems = scanner.nextLine().split(" ");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 0; i < n; i++) {
			int aItem = Integer.parseInt(aItems[i]);
			a[i] = aItem;
		}

		scanner.close();
		int[] arr = leftRotate(a, d, n);
		for (int i = 0; i < n; i++) {
			System.out.println(arr[i]);

		}
	}

	private static int[] leftRotate(int[] a, int d, int n) {
		for (int i = 0; i < d; i++) {
			int temp = a[0];
			for (int j = 0; j < n - 1; j++) {
				a[j] = a[j + 1];
			}
			a[n-1] = temp;
		}
		return a;
	}

	/*
	 * private static void leftByOne(int[] a, int n) { int temp = a[0], i; for
	 * (i = 0; i < n - 1; i++) a[i] = a[i + 1];
	 * 
	 * a[i] = temp;
	 * 
	 * }
	 */
}