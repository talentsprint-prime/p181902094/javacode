package arrayList;

import java.util.Scanner;

public class ListComp {

    public static void main(String[] args) {
        int[][] direction = { { 1, 1 }, { 1, 0 }, { 1, -1 }, { 0, 1 }, { 0, -1 }, { -1, 1 }, { -1, 0 }, { -1, -1 } };
        Scanner sc = new Scanner(System.in);
        String[] nk = sc.nextLine().split(" ");
        int n = Integer.parseInt(nk[0]);
        int k = Integer.parseInt(nk[1]);
        // System.out.println(n + "" + k);
        String[] r_qC_q = sc.nextLine().split(" ");
        int r_q = Integer.parseInt(r_qC_q[0]);
        r_q = n - r_q;
        int c_q = Integer.parseInt(r_qC_q[1]);
        c_q = c_q - 1;
        int[][] obstacles = new int[k][2];

        for (int i = 0; i < k; i++) {
            String[] obstaclesRowItems = sc.nextLine().split(" ");
            sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < 2; j++) {
                int obstaclesItem = Integer.parseInt(obstaclesRowItems[j]);
                obstacles[i][j] = obstaclesItem;
            }
            // sc.close();
        }

        char[][] grid = new char[n][n];
        grid[r_q][c_q] = 'q';
        for (int i = 0; i < k; i++) {

            int px = obstacles[i][0];
            int py = obstacles[i][1];
            grid[n - px][py-1] = 'o';
        }

        int count = 0;

        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (grid[i][j] == 'q') {
                    for (int a = 0; a < 8; a++) {
                        int I = i;
                        int J = j;
                        int x = direction[a][0];
                        int y = direction[a][1];
                        I = I + x;
                        J = J + y;
                        if (I >= 0 && J >= 0) {
                            while ((I < n && J < n) && grid[I][J] != 'o') {
                                //System.out.println(I +""+ J);
                                I = I + x;
                                J = J + y;
                                count++;
                                if (I < 0 || J < 0) {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println(count);
    }
}
