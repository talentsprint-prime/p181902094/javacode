package arrayList;

public class Movie {
	private String Name;
	private String directorName;
	private int duration;
	private int releasedYear;
	
	public Movie() {
		super();
	}

	public Movie(String name, String directorName, int duration, int releasedYear) {
		super();
		Name = name;
		this.directorName = directorName;
		this.duration = duration;
		this.releasedYear = releasedYear;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getDirectorName() {
		return directorName;
	}

	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getReleasedYear() {
		return releasedYear;
	}

	public void setReleasedYear(int releasedYear) {
		this.releasedYear = releasedYear;
	}

	@Override
	public String toString() {
		return "Movie [Name=" + Name + ", directorName=" + directorName + ", duration=" + duration + ", releasedYear="
				+ releasedYear + "]";
	}

}
