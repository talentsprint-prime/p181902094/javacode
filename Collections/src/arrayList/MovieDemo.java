package arrayList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class MovieDemo {
	public static void main(String[] args) {

		List<Movie> MList = new ArrayList<>();
		MList.add(new Movie("CaptainMarvel", "AnnaBoden", 90, 2019));
		MList.add(new Movie("GullyBoy", "ZoyaAkthar", 120, 2019));
		MList.add(new Movie("Padmavat", "SanjayBansali", 180, 2018));
		MList.add(new Movie("Sanju", "RajuHirani", 160, 2018));
		MList.add(new Movie("Raazi", "MeghanaGulzar", 120, 2018));
		MList.add(new Movie("Badla", "sujayGosh", 100, 2019));
		MList.add(new Movie("Dhoni", "NeerajPandey", 165, 2016));
		MList.add(new Movie("Dhangal", "NiteshTiwari", 190, 2016));
		MList.add(new Movie("PK", "RajkumarHirani", 200, 2014));
		MList.add(new Movie("Thamasha", "ImtiazAli", 195, 2015));

		int choice;
		while (true) {

			System.out.println("\n" + "1.Sort by year" + "\n" + "2.Sort by name" + "\n" + "3.Sort by Director name" + "\n" + "4.Exit"+"\n");
			System.out.println("\n" + "Enter the choice");
			Scanner sc = new Scanner(System.in);
			choice = sc.nextInt();
			
			switch (choice) {

			case 1:
				Collections.sort(MList, new SortByYear());
				for (Movie m : MList)
					System.out.println(m);
				break;
			case 2:
				Collections.sort(MList, new SortByName());
				for (Movie m : MList)
					System.out.println(m);
				break;
			case 3:
				Collections.sort(MList, new SortByDirectorName());
				for (Movie m : MList)
					System.out.println(m);
				break;
			case 4:
				System.exit(0);
				break;
			default:
				System.out.println("Enter valid choice");
				break;

			}
		}

	}

}
