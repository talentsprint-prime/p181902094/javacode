package arrayList;

import java.util.Comparator;

public class SortByDirectorName implements Comparator<Movie>{

	@Override
	public int compare(Movie o1, Movie o2) {
		return o1.getDirectorName().compareTo(o2.getDirectorName());
	}

}
