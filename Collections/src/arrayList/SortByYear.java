package arrayList;

import java.util.Comparator;

public class SortByYear implements Comparator<Movie> {

	@Override
	public int compare(Movie o1, Movie o2) {
		if (o1.getReleasedYear() > o2.getReleasedYear())
			return 1;
		else if (o1.getReleasedYear() < o2.getReleasedYear())
			return -1;
		else
			return 0;

	}
}
