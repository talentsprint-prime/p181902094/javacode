package arrayList;
import java.util.List;
import java.util.ArrayList;

public class StudentDemo {
	public static void main(String[] args) {

		List<Student> studentList = new ArrayList<>();

		Student s1 = new Student("shivani", 60, 60, 60);
		studentList.add(s1);
		studentList.add(new Student("haritha", 75, 85, 65));
		studentList.add(new Student("bhavani", 55, 65, 60));

		for (Student s : studentList) {
			System.out.println("Id:" + s.getId() + " name:" + s.getName() + " percentage:" + s.percentage());

		}
	}

}
