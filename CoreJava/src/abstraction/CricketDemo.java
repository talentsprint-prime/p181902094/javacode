package abstraction;

public class CricketDemo {

	public static void main(String[] args) {
		Batsman b1 = new Batsman("Dhoni", 287, 10000, 12000, 70  );
		System.out.println("Average :" + b1.calAverage());
		
		Player p1 = new Bowler("Bhuvneshwar Kumar" ,21 , 63 , 1644 , 3348);		
		System.out.println("Average :" + p1.calAverage());
		
		Bowler b2 = new Bowler("Bhuvneshwar Kumar" ,21 , 63 , 1644 , 3348);	
		System.out.println("Economy : " + b2.calEconomy());
		
		
		
	}

}
