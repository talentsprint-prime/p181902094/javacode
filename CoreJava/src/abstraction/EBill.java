package abstraction;

public class EBill implements Bill {
	public static final int per_unit_charge = 10;
	private int number_of_units;
	
	
	public EBill() {
		super();
	}
	
	public EBill(int number_of_units) {
		super();
		this.number_of_units = number_of_units;
	}
	

	public int getNumber_of_units() {
		return number_of_units;
	}

	public void setNumber_of_units(int number_of_units) {
		this.number_of_units = number_of_units;
	}

	@Override
	public double calculateBill() {
		
		return per_unit_charge * number_of_units;
	}
	@Override
	public void displayBill() {
		System.out.println("=========Electricity BIll=========");
		System.out.println("per_unit_charge = 10 " + "\n" +"number_of_units= " + number_of_units+ "\n" + "BillAmount = " + calculateBill() + "\n");
		
	}
	
	

}
