package abstraction;

public class HDFC implements Bank {
	private int id;
	private double rateOfInterest;
	private String branch;
	private Customer customer;
	public static final double rate_of_interest = 8;

	public HDFC() {
		super();
	}

	public HDFC(int id, String branch, Customer customer) {
		super();
		this.id = id;
		this.branch = branch;
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "HDFC [id=" + id + ", rateOfInterest=" + rateOfInterest + ", branch=" + branch + ", customer=" + customer
				+ ", calSI()=" + calSI() + "]";
	}

	public void setCust(Customer cust) {
		this.customer = customer;
	}

	public Customer getCust() {
		return customer;
	}

	@Override
	public double calSI() {

		return ((customer.getPrinciple() * customer.getTenure() * rate_of_interest) / 100);

	}
}
