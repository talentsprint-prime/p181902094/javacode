package abstraction;

public class ICICI implements Bank {
	private int id;
	private String branch;
	private Customer customer;
	public static final double rate_of_interest = 7.6;

	public ICICI() {
		super();
	}

	public ICICI(int id, String branch, Customer cust) {
		super();
		this.id = id;
		this.branch = branch;
		this.customer = customer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Customer getCust() {
		return customer;
	}

	public void setCust(Customer cust) {
		this.customer = cust;
	}

	@Override
	public double calSI() {

		return (customer.getPrinciple() * customer.getTenure() * rate_of_interest) / 100;
	}

	@Override
	public String toString() {
		return "ICICI [id=" + id + ", branch=" + branch + ", customer=" + customer + ", calSI()=" + calSI() + "]";
	}

}
