package abstraction;

public class WBill implements Bill {
	public static final int per_unit_charge = 5;
	private int number_of_units;

	public WBill() {
		super();
	}

	public WBill(int number_of_units) {
		super();
		this.number_of_units = number_of_units;
	}
	
	public int getNumber_of_units() {
		return number_of_units;
	}

	public void setNumber_of_units(int number_of_units) {
		this.number_of_units = number_of_units;
	}

	@Override
	public double calculateBill() {
		return per_unit_charge * number_of_units;

	}

	@Override
	public void displayBill() {
		System.out.println("=========Water BIll==========");
		System.out.println("per_unit_charge = 5 " + "\n" +"number_of_units = " + number_of_units+ "\n" + "BillAmount = :" + calculateBill() + "\n");

	}

}
