package association;

public class Employee {
	private int id;
	private String name;
	private double salary;
	private String designation;
	private Address add;
	private static int idGenerator = 100;

	public Employee() {
		super();
		add = new Address();
		this.id = ++idGenerator;

	}

	public Employee(String name, double salary, String designation, Address add) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.salary = salary;
		this.designation = designation;
		this.add = add;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public int getId() {
		return id;
	}

	public static int getIdGenerator() {
		return idGenerator;
	}

	public Address getAdd() {
		return add;
	}

	public void setAdd(Address add) {
		this.add = add;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + ", designation=" + designation
				+ ", add=" + add + "]";
	}

}
