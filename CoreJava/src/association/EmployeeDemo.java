package association;

public class EmployeeDemo {
	public static void main(String[] args) {
		Address a1 = new Address(201,"friends colony","Hyderabad", "Telangana", 500050);
		Employee e1 = new Employee("Shivani", 80000, "developer", a1);
		System.out.println(e1);
		Address a2 = new Address(101,"shilpa enclave","Hyderabad", "Telangana", 500050);
		e1.setAdd(a2);
		System.out.println(e1);


	}
}
