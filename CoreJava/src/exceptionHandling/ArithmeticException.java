package exceptionHandling;

public class ArithmeticException {

	public static void main(String[] args) {
		int a = 10;
		int b = 0;

		int sum = 0, mul = 0, div = 0;
		try {

			sum = a + b;
			div = a / b;
			mul = a * b;

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		System.out.println(sum);
		System.out.println(mul);
		System.out.println(div);
	}

}
