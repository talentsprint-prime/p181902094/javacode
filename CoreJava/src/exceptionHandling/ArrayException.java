package exceptionHandling;

public class ArrayException {
	public static void main(String[] args) {
		int arr[] = {1,2,3,4,5};
		for (int i = 0; i <= 5; i++) {
			try {
				System.out.println(arr[i]);
			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

}
