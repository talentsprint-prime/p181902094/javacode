package exceptionHandling;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyFile {

	public static void main(String[] args) throws IOException {
		int i = 0;

		FileReader fr = null;
		FileWriter fw = null;

		try {
			fr = new FileReader("country.txt");
			fw = new FileWriter("CopyCountry.txt");
			while ((i = fr.read()) != -1) {
				fw.write(i);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} finally {
			fr.close();
			fw.close();
		}

	}

}
