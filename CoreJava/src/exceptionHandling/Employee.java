package exceptionHandling;


public class Employee implements Comparable<Employee> {
	private int id;
	private String name;
	private String designation;
	private int salary;
	public Employee() {
		super();
	}
	public Employee(int id,String name,int salary,String designation) {
		super();
		this.id = id;
		this.name = name;
		this.designation = designation;
		this.salary = salary;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", designation=" + designation + ", salary=" + salary + "]";
	}
	@Override
	public int compareTo(Employee e) {
		if(this.getSalary() > e.getSalary())
			return -1;
		else if(this.getSalary() < e.getSalary())
			return 1;
		else
			return 0;
	}

}
