package exceptionHandling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class EmployeeBuffer {

	public static void main(String[] args) throws IOException {
		FileReader fr = null;
		BufferedReader br = null;
		String line = "";
		List<Employee> empList = new ArrayList<>();

		try {
			fr = new FileReader("Employee.txt");
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				String arr[] = line.split(" ");
				int id = Integer.parseInt(arr[0]);
				String name = arr[1];
				int salary = Integer.parseInt(arr[2]);
				String designation = arr[3];
				empList.add(new Employee(id, name, salary, designation));
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if(fr != null)
			fr.close();
			if(br != null)
			br.close();
		}
		Collections.sort(empList);// using comparable descending order
		//Collections.sort(empList, new SortBySalary());//using comparator ascending order
		for (Employee e : empList)
			System.out.println(e);

	}

}
