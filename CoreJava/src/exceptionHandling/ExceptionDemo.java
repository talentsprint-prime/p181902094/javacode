package exceptionHandling;

public class ExceptionDemo {

	public static void main(String[] args) {
		int a = 10;
		int b = 0;
		try{
		compute(a,b);
		}
		catch(Exception e){
			System.out.println(e.getMessage());
		}
	}

	public static void compute(int a, int b){
		div(a,b);
	}
	
	public static void div(int a , int b){
		int result = a/b;
		System.out.println("result" + result);
	}
}
