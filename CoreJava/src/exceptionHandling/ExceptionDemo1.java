package exceptionHandling;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ExceptionDemo1 {

	public static void main(String[] args) {
		
		FileInputStream fis = null;
		
		try {
			fis = new FileInputStream("Hello.txt");
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}

	}

}
