package exceptionHandling;

import java.io.File;
import java.io.IOException;

public class FileDemo {
	public static void main(String args[]) throws IOException{
		File f = new File("Hello.txt");
		System.out.println(f.createNewFile());
		System.out.println(f.getParentFile());
		System.out.println(f.length());
		File f1 = new File("Hello1.txt");
		f.renameTo(f1);
		f1.delete();
		
	}
}
