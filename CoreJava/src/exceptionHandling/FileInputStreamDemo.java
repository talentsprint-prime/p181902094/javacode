package exceptionHandling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileInputStreamDemo {

	public static void main(String[] args) throws IOException {
		int i = 0;
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("Input.txt");
			while ((i = fis.read()) != -1) {
				System.out.print((char) i);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} finally {
			fis.close();
		}

	}

}
