package exceptionHandling;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo {

	public static void main(String[] args) throws IOException {

		FileOutputStream fio = null;
		String name = " Reddy ";

		try {
			fio = new FileOutputStream("Output.txt");

			byte[] b = { 'S', 'h', 'i' , 'v' , 'a' , 'n' ,'i' };
			fio.write(b);

			fio.write(name.getBytes());
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} finally {
			fio.close();

		}

	}

}
