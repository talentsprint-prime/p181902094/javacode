package exceptionHandling;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReadWriteDemo {

	public static void main(String[] args) throws IOException {
		int i = 0;
		FileReader fr = null;
		FileWriter fw = null;
		
		//FileReader
		try {
			fr = new FileReader("Input.txt");
			while ((i = fr.read()) != -1) {
				System.out.print((char) i);
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} finally {
			fr.close();
		}
		
		//FleWriter in append mode
		fw = new FileWriter("Output.txt",true);
		fw.write(".Y");// can also be fw.write()
		fw.close();
		
	}

}
