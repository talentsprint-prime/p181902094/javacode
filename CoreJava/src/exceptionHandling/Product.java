package exceptionHandling;

public class Product {
	private int id;
	private double price;
	private double weight;
	private static int idGenerator = 100;
	
	public Product() {
		super();
		
	}

	public Product(double price, double weight) {
		super();
		this.price = price;
		this.weight = weight;
		this.id = idGenerator++;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", price=" + price + ", weight=" + weight + "]";
	}

}
