package exceptionHandling;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProductMain {

	public static void main(String[] args) {
		List<Product> productList = new ArrayList<>();

		productList.add(new Product(25, 153.5));
		productList.add(new Product(92, 230));
		productList.add(new Product(257, 168));
		productList.add(new Product(392, 267));
		productList.add(new Product(420, 195));
		productList.add(new Product(630, 365.42));
		productList.add(new Product(812, 257.4));
		productList.add(new Product(110, 199.5));
		productList.add(new Product(321, 200.001));
		productList.add(new Product(250, 153.89));

		Iterator<Product> iterator = productList.iterator();

		while (iterator.hasNext()) {
			Product p = iterator.next();
			try {
				System.out.println(checkValidProduct(p));
			} catch (InvalidProductException e1) {
				iterator.remove();
				System.out.println(e1.getMessage());

			}
		}
		System.out.println("\nList of valid Products \n");
		for (Product p1 : productList) {
			System.out.println(p1);
		}

	}

	public static String checkValidProduct(Product p) throws InvalidProductException {
		String str = "";
		if (p.getWeight() < 200) {
			str = p.getId() + " : Invalid product";
			throw new InvalidProductException(str);
		} else {
			str = str = p.getId() + " : Valid product";
			return str;

		}

	}
}
