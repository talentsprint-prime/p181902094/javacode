package exceptionHandling;

public class Student {
	private long StudentID;
	private String name;
	
	
	public Student() {
		super();
	}


	public Student(long StudentID, String name) {
		super();
		
		this.StudentID = StudentID;
		this.name = name;
	}



	public long getStudentID() {
		return StudentID;
	}


	public void setStudentID(long StudentID) {
		this.StudentID = StudentID;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "Student [StudentID=" + StudentID + ", name=" + name + "]";
	}
	
}
