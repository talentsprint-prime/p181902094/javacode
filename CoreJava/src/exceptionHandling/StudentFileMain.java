package exceptionHandling;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentFileMain {

	public static void main(String[] args) throws IOException {
		FileReader fr = null;
		BufferedReader br = null;
		String line = "";
		List<StudentFile> studentList = new ArrayList<>();

		try {
			fr = new FileReader("student.csv");
			br = new BufferedReader(fr);

			while ((line = br.readLine()) != null) {
				String arr[] = line.split(",");
				String name = arr[0];
				int sub1 = Integer.parseInt(arr[1]);
				int sub2 = Integer.parseInt(arr[2]);
				int sub3 = Integer.parseInt(arr[3]);
				studentList.add(new StudentFile(name, sub1, sub2, sub3));
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} finally {
			if(fr != null)
			fr.close();
			if(br != null)
			br.close();
		}
		Collections.sort(studentList);
		for (StudentFile s : studentList)
			System.out.println(s);

	}

}
