package exceptionHandling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class StudentMain {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Student ID");
		long id = sc.nextLong();
		Student v = new Student();

		Student v1 = new Student(123456, "Shivani");
		Student v2 = new Student(123457, "Bhavani");
		Student v3 = new Student(123458, "Alekhya");

		HashMap<Long , Student> studentMap = new HashMap<>();

		studentMap.put(v1.getStudentID() , v1);
		studentMap.put(v2.getStudentID(), v2);
		studentMap.put(v3.getStudentID() , v3);

		try {
			System.out.println(searchStudent(studentMap,id));
		} catch (StudentNotFoundException e1) {
			System.out.println(e1.getMessage());
		}

	}

	public static String searchStudent(HashMap<Long, Student> studentMap, Long id)throws StudentNotFoundException {
		String str = "";
		if (studentMap.containsKey(id)) {
			Student v  = studentMap.get(id);
			return v.toString();
		}

		else {
			throw new StudentNotFoundException("Student not found");
		}
	}

}
