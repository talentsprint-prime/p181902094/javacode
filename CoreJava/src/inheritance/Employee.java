package inheritance;

public class Employee {
	private int id;
	private double salary;
	private long phoneNumber;
	private String name;
	private String address;
	private static int idGenerator = 100;

	public Employee() {
		super();
		this.id = ++idGenerator;

	}

	public Employee(double salary, long phoneNumber, String name, String address) {
		super();
		this.id = ++idGenerator;
		this.salary = salary;
		this.phoneNumber = phoneNumber;
		this.name = name;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public static int getIdGenerator() {
		return idGenerator;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", salary=" + salary + ", phoneNumber=" + phoneNumber + ", name=" + name
				+ ", address=" + address + "]";
	}
	
	public void work(){
		System.out.println("I am an employee !!Doing some work");
	}
	
	

}
