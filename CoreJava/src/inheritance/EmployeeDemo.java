package inheritance;

public class EmployeeDemo {

	public static void main(String[] args) {
		Programmer p1 = new Programmer(25000 , 7981632791l , "Shivani" , "Gachibowli" , " Java");
		System.out.println(p1);
		p1.work();
		
		System.out.println("Programmer : " + p1.getId());
		Employee emp1 = new Employee(25600 , 7981632799l , "shiv" , "chandanagr");
		emp1.work();
		Employee emp2 = new Programmer(35000 , 7981632798l , "Shiva" , "Gachibowli" , " C++");
		emp2.work();
		Employee emp3 = new Manager(35000 , 7981632798l , "Shiva" , "Gachibowli" , "manager");
		System.out.println(emp3);
	}

}
