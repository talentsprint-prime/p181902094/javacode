package inheritance;

public class Manager extends Employee {
	private String manager;

	public Manager() {
		super();
	}

	public Manager(double salary,long phoneNumber,String name, String address,String manager) {
		super(salary, phoneNumber,name,address);
		this.manager = manager;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Manager [manager=" + manager + ", toString()=" + super.toString() + "]";
	}
}