package inheritance;

public class PersonDemo {

	public static void main(String[] args) {
		Person p1 = new Student("Shivani", "hyderabad", "CSE", 2018, 87000);
		System.out.println(p1);
		Person p2 = new Staff("Alekhya", "nizampet", "VNR", 60000);
		System.out.println(p2);
	}

}
