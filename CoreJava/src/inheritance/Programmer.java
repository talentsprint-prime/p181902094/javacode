package inheritance;

public class Programmer extends Employee {

	private String programmingLang;

	public Programmer() {
		super();

	}

	public Programmer(double salary, long phoneNumber, String name, String address, String programmingLang) {

		super(salary, phoneNumber, name, address);
		this.programmingLang = programmingLang;
	}

	public String getProgrammingLang() {
		return programmingLang;
	}

	public void setProgrammingLang(String programmingLang) {
		this.programmingLang = programmingLang;
	}

	@Override
	public String toString() {
		return "Programmer [programmingLang=" + programmingLang + ", toString()=" + super.toString() + "]";
	}

	public void work() {
		System.out.println("I am a programmer!! ");
	}

}
