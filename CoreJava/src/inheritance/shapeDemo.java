package inheritance;

public class shapeDemo {

	public static void main(String[] args) {
		
		Circle c1 = new Circle(3, "red", true);
		
		System.out.println(c1);
		
		Rectangle r1 = new Rectangle(2 , 3 , "red" , true);
		
		System.out.println(r1);
		
	}

}
