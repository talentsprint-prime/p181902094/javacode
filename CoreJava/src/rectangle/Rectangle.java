package rectangle;

public class Rectangle {
	int id;
	int length;
	int width;

	Rectangle() {

	}

	Rectangle(int id, int length, int width) {
		setId(id);
		setLength(length);
		setWidth(width);

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	int area(int length, int width) {

		int area = length * width;
		return area;
	}

	int perimeter(int length, int width) {

		int perimeter = 2 * (length + width);
		return perimeter;
	}

}
