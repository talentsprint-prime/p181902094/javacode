package rectangle;

import java.util.Scanner;

public class RectangleMain {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int l = sc.nextInt();
		int w = sc.nextInt();

		Rectangle ob = new Rectangle();

		String str = "";
		str = str + "length : " + l + "\n" + "width : " + w + "\n" + "area : " + ob.area(l, w) + "\n" + "perimeter : "
				+ ob.perimeter(l, w);

		System.out.println(str);

		Rectangle ob2 = new Rectangle(2, 12, 12);
		System.out.println("ID: " + ob2.getId());//ob2.id gives the same value
		System.out.println("Length: " + ob2.length);
		System.out.println("Width: " + ob2.width);

		System.out.println("Area: " + ob2.area(ob2.length, ob2.width) + "\n" + "Perimeter: "
				+ ob2.perimeter(ob2.length, ob2.width));

	}

}
