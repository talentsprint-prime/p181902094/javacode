package exam;

import java.util.Scanner;

public class NextPalindrome {

	public static void main(String[] args) {
		int num;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		num = sc.nextInt();
	
		System.out.println(nextPalindrome(num));

	}

	public static int reverseDigit(int number) {
		
		int rev = 0, digit;
		
		while (number > 0) {
			digit = number % 10;
			rev = rev * 10 + digit;
			number = number / 10;
		}
		return rev;
	}
	
	public static int countDigits(int num) {
		int count = 0;
		while (num != 0) {
			num /= 10;
			++count;

		}
		return count;
	}

	public static int nextPalindrome(int num) {
		int n;
		for (n = num; n <= num + Math.pow(10, countDigits(num)); n++) {
			if (n == reverseDigit(n)) {
				System.out.println("Next palindrome is ");
				break;
			}

		}
		return n;
	}

	
}
