package exam;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ProdutOfFactors {

	public static void main(String[] args) {
		Map<Character,Integer> freqCount = frequencyCount("ELEMENTS");
		/*Map<Character,Integer> freqCount1 = frequencyCount("ELEMENTS");
		if(freqCount.equals(freqCount1)){
			System.out.println("Anagram");
		}*/
		for(Character key : freqCount.keySet()){
			System.out.println(key + "" + freqCount.get(key));
		}
		
	}
	public static Map<Character, Integer> frequencyCount(String str){
		Map<Character, Integer> freqCount = new LinkedHashMap<Character,Integer>();
		
		if(str == null)
			return freqCount;
		str = str.trim();
		if(str.length() == 0)
			return freqCount;
			
		str = str.toUpperCase();
		for( char ch : str.toCharArray()){
			if(Character.isLetter(ch)){
				if(freqCount.containsKey(ch)){
					freqCount.put(ch, (Integer)freqCount.get(ch)+1);
				}else{
					freqCount.put(ch,1);
				}
			}
		}
		return freqCount;
	}
					
}
