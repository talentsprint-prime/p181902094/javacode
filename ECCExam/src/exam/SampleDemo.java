package exam;

public class SampleDemo{
	char puzzle[][] = { { 'a', 'b', ' ', 'c', 'd' }, { 'e', 'f', 'g', 'h', 'i' }, { 'j', 'k', 'l', 'm', 'n' },
			{ 'o', 'p', 'q', 'r', 's' }, { 't', 'u', 'v', 'w', 'x' } };
	
	public boolean isBestCase(char[][] puzzle) {
		String str = "";
		String finalConfig = "abcdefghijklmnopqrstuvwx";
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				str += puzzle[i][j];
			}
		}
		str = str.trim();

		if (str.equals(finalConfig))
			return true;
		else
			return false;

	}
}
