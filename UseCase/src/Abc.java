import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Abc {
	static char[][] board = { { '_', '_', '_', '_', '_', '_', '_', '_' }, { '_', '_', '_', '_', '_', '_', '_', '_' },
			{ '_', '_', '_', '_', '_', '_', '_', '_' }, { '_', '_', '_', 'W', 'B', '_', '_', '_' },
			{ '_', '_', '_', 'B', 'W', '_', '_', '_' }, { '_', '_', '_', '_', '_', '_', '_', '_' },
			{ '_', '_', '_', '_', '_', '_', '_', '_' }, { '_', '_', '_', '_', '_', '_', '_', '_' } };

	static int[][] direction = { { 1, 1 }, { 1, 0 }, { 1, -1 }, { 0, 1 }, { 0, -1 }, { -1, 1 }, { -1, 0 }, { -1, -1 } };

	public static void main(String[] args) {
		System.out.println(findPlaceableLocations('B', 'W'));
		System.out.println("done");

	}

	public static HashSet<Point> findPlaceableLocations(char player, char opponent) {
		HashSet<Point> placeablePositions = new HashSet<>();
		for (int i = 0; i < 8; ++i) {

			for (int j = 0; j < 8; ++j) {

				if (board[i][j] == opponent) {
					int I = i, J = j;
					for (int k = 0; k < 8; k++) {

						int x = direction[k][0];
						int y = direction[k][1];
						i = I;
						j = J;
						// System.out.println(i +" + " +j);
						if (i - x >= 0 && j - y >= 0 && board[i - x][j - y] == '_') {
							i = i + x;
							j = j + y;
							while (i < 7 && j < 7 && board[i][j] == opponent) {
								if (x == 1)
									i++;
								if (y == 1)
									j++;
							}
							if (i <= 7 && j <= 7 && board[i][j] == player) {
								System.out.println(i + " + " + j);
								placeablePositions.add(new Point(I - x, J - y));
								System.out.println(placeablePositions.size());

							}

						}

					}
				}
			}
		}
		return placeablePositions;
	}
}
