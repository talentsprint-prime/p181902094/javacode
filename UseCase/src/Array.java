import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Array {
	
	public static void main(String[] args) {
		char[][] board = { { '_', '_', '_', '_', '_', '_', '_', '_', }, { '_', '_', '_', '_', '_', '_', '_', '_', },
				{ '_', '_', '_', '_', '_', '_', '_', '_', }, { '_', '_', '_', 'W', 'B', '_', '_', '_', },
				{ '_', '_', '_', 'B', 'W', '_', '_', '_', }, { '_', '_', '_', '_', '_', '_', '_', '_', },
				{ '_', '_', '_', '_', '_', '_', '_', '_', }, { '_', '_', '_', '_', '_', '_', '_', '_', }, };
		displayBoard(board);
		Scanner sc = new Scanner(System.in);
		char player = sc.next().charAt(0);

		othello(player, board);

	}

	public static void othello(char player, char[][] board) {
		char opponent;

		boolean b = true;
		displayBoard(board);
		while (b) {
			System.out.println("player is " + player);
			if (player == 'W')
				opponent = 'B';
			else
				opponent = 'W';
			System.out.println("Placeable positions for player ");
			System.out.println(findPlaceableLocations(board, player, opponent));
			Scanner sc = new Scanner(System.in);
			System.out.println("enter zero to exit");
			int x = sc.nextInt();
			if (x == 0) {
				System.out.println("game end");
				break;
			} else {
				int y = sc.nextInt();
				Point p = new Point(x - 1, y - 1);
				displayBoard(placeMove(board, p, player, opponent));

				player = opponent;
				countNumberOfWB(board);
				if (!allBoxesFilled(board)) {
					b = false;
				}
			}

		}

	}

	public static void displayBoard(char[][] board) {
		System.out.print("\n  ");
		for (int i = 0; i < 8; ++i)
			System.out.print((i + 1) + " ");
		System.out.println();
		for (int i = 0; i < 8; ++i) {
			System.out.print((i + 1) + " ");
			for (int j = 0; j < 8; ++j)
				System.out.print(board[i][j] + " ");
			System.out.println();
		}
		System.out.println();
	}

	public static char[][] placeMove(char[][] board, Point p, char player, char opponent) {
		int i = p.x, j = p.y;
		board[i][j] = player;
		int I = i, J = j;

		if (i - 1 >= 0 && j - 1 >= 0 && board[i - 1][j - 1] == opponent) {
			i = i - 1;
			j = j - 1;
			while (i > 0 && j > 0 && board[i][j] == opponent) {
				i--;
				j--;
			}
			if (i >= 0 && j >= 0 && board[i][j] == player) {
				while (i != I - 1 && j != J - 1)
					board[++i][++j] = player;
			}
		}
		i = I;
		j = J;
		if (i - 1 >= 0 && board[i - 1][j] == opponent) {
			i = i - 1;
			while (i > 0 && board[i][j] == opponent)
				i--;
			if (i >= 0 && board[i][j] == player) {
				while (i != I - 1)
					board[++i][j] = player;
			}
		}
		i = I;
		if (i - 1 >= 0 && j + 1 <= 7 && board[i - 1][j + 1] == opponent) {
			i = i - 1;
			j = j + 1;
			while (i > 0 && j < 7 && board[i][j] == opponent) {
				i--;
				j++;
			}
			if (i >= 0 && j <= 7 && board[i][j] == player) {
				while (i != I - 1 && j != J + 1)
					board[++i][--j] = player;
			}
		}
		i = I;
		j = J;
		if (j - 1 >= 0 && board[i][j - 1] == opponent) {
			j = j - 1;
			while (j > 0 && board[i][j] == opponent)
				j--;
			if (j >= 0 && board[i][j] == player) {
				while (j != J - 1)
					board[i][++j] = player;
			}
		}
		j = J;
		if (j + 1 <= 7 && board[i][j + 1] == opponent) {
			j = j + 1;
			while (j < 7 && board[i][j] == opponent)
				j++;
			if (j <= 7 && board[i][j] == player) {
				while (j != J + 1)
					board[i][--j] = player;
			}
		}
		j = J;
		if (i + 1 <= 7 && j - 1 >= 0 && board[i + 1][j - 1] == opponent) {
			i = i + 1;
			j = j - 1;
			while (i < 7 && j > 0 && board[i][j] == opponent) {
				i++;
				j--;
			}
			if (i <= 7 && j >= 0 && board[i][j] == player) {
				while (i != I + 1 && j != J - 1)
					board[--i][++j] = player;
			}
		}
		i = I;
		j = J;
		if (i + 1 <= 7 && board[i + 1][j] == opponent) {
			i = i + 1;
			while (i < 7 && board[i][j] == opponent)
				i++;
			if (i <= 7 && board[i][j] == player) {
				while (i != I + 1)
					board[--i][j] = player;
			}
		}
		i = I;

		if (i + 1 <= 7 && j + 1 <= 7 && board[i + 1][j + 1] == opponent) {
			i = i + 1;
			j = j + 1;
			while (i < 7 && j < 7 && board[i][j] == opponent) {
				i++;
				j++;
			}
			if (i <= 7 && j <= 7 && board[i][j] == player)
				while (i != I + 1 && j != J + 1)
					board[--i][--j] = player;
		}
		return board;
	}

	public static HashSet<Point> findPlaceableLocations(char[][] board, char player, char opponent) {
		HashSet<Point> placeablePositions = new HashSet<>();
		for (int i = 0; i < 8; ++i) {
			for (int j = 0; j < 8; ++j) {
				if (board[i][j] == opponent) {
					int I = i, J = j;
					if (i - 1 >= 0 && j - 1 >= 0 && board[i - 1][j - 1] == '_') {
						i = i + 1;
						j = j + 1;
						while (i < 7 && j < 7 && board[i][j] == opponent) {
							i++;
							j++;
						}
						if (i <= 7 && j <= 7 && board[i][j] == player)
							placeablePositions.add(new Point(I, J));
					}
					i = I;
					j = J;
					if (i - 1 >= 0 && board[i - 1][j] == '_') {
						i = i + 1;
						while (i < 7 && board[i][j] == opponent)
							i++;
						if (i <= 7 && board[i][j] == player)
							placeablePositions.add(new Point(I, J + 1));
					}
					i = I;
					if (i - 1 >= 0 && j + 1 <= 7 && board[i - 1][j + 1] == '_') {
						i = i + 1;
						j = j - 1;
						while (i < 7 && j > 0 && board[i][j] == opponent) {
							i++;
							j--;
						}
						if (i <= 7 && j >= 0 && board[i][j] == player)
							placeablePositions.add(new Point(I, J + 2));
					}
					i = I;
					j = J;
					if (j - 1 >= 0 && board[i][j - 1] == '_') {
						j = j + 1;
						while (j < 7 && board[i][j] == opponent)
							j++;
						if (j <= 7 && board[i][j] == player)
							placeablePositions.add(new Point(I + 1, J));
					}
					j = J;
					if (j + 1 <= 7 && board[i][j + 1] == '_') {
						j = j - 1;
						while (j > 0 && board[i][j] == opponent)
							j--;
						if (j >= 0 && board[i][j] == player)
							placeablePositions.add(new Point(I + 1, J + 2));
					}
					j = J;
					if (i + 1 <= 7 && j - 1 >= 0 && board[i + 1][j - 1] == '_') {
						i = i - 1;
						j = j + 1;
						while (i > 0 && j < 7 && board[i][j] == opponent) {
							i--;
							j++;
						}
						if (i >= 0 && j <= 7 && board[i][j] == player)
							placeablePositions.add(new Point(I + 2, J));
					}
					i = I;
					j = J;
					if (i + 1 <= 7 && board[i + 1][j] == '_') {
						i = i - 1;
						while (i > 0 && board[i][j] == opponent)
							i--;
						if (i >= 0 && board[i][j] == player)
							placeablePositions.add(new Point(I + 2, J + 1));
					}
					i = I;
					if (i + 1 <= 7 && j + 1 <= 7 && board[i + 1][j + 1] == '_') {
						i = i - 1;
						j = j - 1;
						while (i > 0 && j > 0 && board[i][j] == opponent) {
							i--;
							j--;
						}
						if (i >= 0 && j >= 0 && board[i][j] == player)
							placeablePositions.add(new Point(I + 2, J + 2));
					}
					i = I;
					j = J;
				}
			}
		}
		return placeablePositions;
	}

	static public void countNumberOfWB(char[][] board) {
		int count = 0, count1 = 0;
		for (int i = 0; i < board[0].length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j] == 'W') {
					count++;
				}
				if (board[i][j] == 'B') {
					count1++;
				}
			}
		}
		System.out.println("White : " + count + " Black :" + count1);
	}

	public static boolean allBoxesFilled(char board[][]) {
		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				if (board[x][y] == '-')
					return false;
			}
		}
		return true;
	}
}