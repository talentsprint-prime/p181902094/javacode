import java.util.Scanner;

public class CrossWordPuzzle {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter puzzle dimensions");
		int row = sc.nextInt();
		int col = sc.nextInt();

		char table[][] = initializeGrid(row,col);

		char crossword[][] = crossWord(table,row,col);
		int cnts[][] = intializeNumber(crossword);

		System.out.println(acrossWords(crossword, cnts));
		System.out.println(downWords(crossword, cnts));
	}
	
	public static char[][] initializeGrid(int row , int col){
		
		char table[][] = new char[row][col];

		for (int i = 0; i < table.length; i++) {
			String str = sc.next();
			for (int j = 0; j < table[0].length; j++)
				table[i][j] = str.charAt(j);
		}
		return table;
	}

	public static int[][] intializeNumber(char[][] crossword) {
		int[][] gridNumbers = new int[crossword.length][crossword[0].length];

		int count = 1;
		for (int i = 1; i < crossword.length; i++) {
			for (int j = 1; j < crossword[i].length; j++) {
				if (crossword[i][j] != '*') {
					if ((crossword[i][j - 1] == '*' || crossword[i - 1][j] == '*')) {
						gridNumbers[i - 1][j - 1] = count++;
					}
				}
			}
		}
		return gridNumbers;
	}

	public static char[][] crossWord(char[][] table,int row,int col) {

		char[][] crossword = new char[row+2][col+2];

		for (int j = 0; j < crossword[0].length; j++) {
			crossword[0][j] = '*';
			crossword[crossword.length - 1][j] = '*';
		}
		for (int i = 0; i < crossword.length; i++) {
			crossword[i][0] = '*';
			crossword[i][crossword[0].length - 1] = '*';
		}

		for (int i = 1; i < crossword.length - 1; i++) {
			for (int j = 1; j < crossword[0].length - 1; j++) {
				crossword[i][j] = table[i - 1][j - 1];
			}
		}

		return crossword;
	}

	public static String acrossWords(char[][] crossword, int[][] gridNumbers) {
		System.out.println("Across :");
		String aOut = "";
		for (int i = 1; i < crossword.length; i++) {
			for (int j = 1; j < crossword[0].length; j++) {
				if (gridNumbers[i - 1][j - 1] != 0 && crossword[i][j] != '*' && crossword[i][j - 1] == '*')
					aOut += aOutput(i, j, gridNumbers, crossword);
			}
		}
		return aOut;
	}

	static String aOutput(int x, int y, int[][] gridNumbers, char[][] crossword) {
		String str = "";
		str = str + gridNumbers[x - 1][y - 1] + ".";
		for (int i = y; i < crossword[0].length && crossword[x][i] != '*'; i++) {
			str = str + crossword[x][i];
		}
		str = str + "\n";
		return str;
	}

	public static String downWords(char[][] crossword, int[][] gridNumbers) {
		System.out.println("Down :");
		String dOut = "";
		for (int i = 1; i < crossword.length; i++) {
			for (int j = 1; j < crossword[0].length; j++) {

				if (gridNumbers[i - 1][j - 1] != 0 && crossword[i][j] != '*' && crossword[i - 1][j] == '*')
					dOut += dOutput(i, j, gridNumbers, crossword);
			}
		}
		return dOut;
	}

	static String dOutput(int x, int y, int[][] gridNumbers, char[][] crossword) {
		String str = "";
		str = str + gridNumbers[x - 1][y - 1] + ".";
		for (int i = x; i < crossword.length && crossword[i][y] != '*'; i++) {
			str = str + crossword[i][y];
		}
		str = str + "\n";
		return str;
	}

}
