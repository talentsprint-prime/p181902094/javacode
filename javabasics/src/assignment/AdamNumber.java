package assignment;

public class AdamNumber {
	public static void main(String[] args) {
		int num = 20;
		System.out.println(isAdamNumber(num));
	}

	public static boolean isAdamNumber(int num) {
		int num1 = getReverse(num);
		if (getSquare(num) == getReverse(getSquare(num1))) {

			return true;
		} else
			return false;

	}

	public static int getReverse(int n) {
		int digit;
		int reversenumber = 0;

		while (n > 0) {
			digit = n % 10;
			reversenumber = (reversenumber * 10) + digit;
			n = n / 10;
		}
		return reversenumber;

	}

	public static int getSquare(int n) {
		int square = n * n;

		return square;

	}
}
