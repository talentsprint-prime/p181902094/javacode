package assignment;

public class AlphabetFibonacci {
	public static void main(String[] args) {
		String str = "MAN";
		System.out.println(fibonacciSum(str));

	}

	public static int fibonacciSum(String str1) {
		int sum = 0;
		int n;
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		
		int[] arr = fibonacciSeries();
		
		for (int i = 0; i < str1.length(); i++) {
			n = alphabet.indexOf(str1.charAt(i));
			sum += arr[n];
		}
		return sum;

	}

	public static int[] fibonacciSeries() {
		int[] arr = new int[26];

		for (int i = 2; i <= 25; i++) {
			arr[0] = 0;
			arr[1] = 1;
			arr[i] = arr[i - 1] + arr[i - 2];
		}

		return arr;
	}

}