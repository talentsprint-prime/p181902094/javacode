package assignment;

public class AlphabetFibonacci1 {

	public static void main(String[] args) {

		String s = "MAN";

		System.out.println(givenString(s));
	}

	public static int givenString(String str) {

		int n;
		int sum = 0;
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		for (int i = 0; i < str.length(); i++) {
			n = alphabet.indexOf(str.charAt(i));
			sum += getNthTermOfFibonacciSeries(n);
		}
		return sum;

	}

	public static int getNthTermOfFibonacciSeries(int n) {

		if (n <= 1)
			return n;

		return getNthTermOfFibonacciSeries(n - 1) + getNthTermOfFibonacciSeries(n - 2);
	}

}
