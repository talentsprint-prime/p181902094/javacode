package assignment;

import java.util.Scanner;

public class AlphabetSegregation {

	public static void main(String[] args) {

		String str;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the text");
		str = sc.nextLine();

		System.out.println(groupingAlphabets(str));
	}

	public static String groupingAlphabets(String str) {

		String Alphabet = "";
		String SpecialChar = "";

		for (int i = 0; i <= str.length() - 1; i++) {
			char ch = str.charAt(i);
			if (Character.isUpperCase(str.charAt(i)) || Character.isLowerCase(str.charAt(i))) {
				Alphabet = Alphabet + str.charAt(i);
			} else if ((int) str.charAt(i) >= 33 && (int) str.charAt(i) <= 47) {
				SpecialChar = SpecialChar + str.charAt(i);
			}

		}
		str = SpecialChar + Alphabet;
		return str;

	}

}
