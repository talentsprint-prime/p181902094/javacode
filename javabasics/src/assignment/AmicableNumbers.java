package assignment;

public class AmicableNumbers {

	public static void main(String[] args) {
		System.out.println(amicablePairs(220, 300));
	}

	public static String amicablePairs(int start, int limit) {
		String str = "";

		for (int i = start; i <= limit; i++) {
			
			int num = findingFactorsSum(i);
			
			if (num > i && num < limit) {
				if (findingFactorsSum(num) == i) {
					str = str + i + ":" + num + "\n";
				}
			}
		}
		System.out.println(str);
		return str;
	}

	public static int findingFactorsSum(int num) {
		int sum = 0;

		for (int i = 2; i * i <= num; i++) {
			if (i * i == num)
				sum = sum + i;
			else if (num % i == 0) {
				sum = sum + i + num / i;
			}
		}
		return sum + 1;
	}

}
