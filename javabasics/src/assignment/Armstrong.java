package assignment;

import java.util.Scanner;

public class Armstrong {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the range");
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		
		System.out.println(isArmstrong(num1, num2));

	}

	public static String isArmstrong(int num1, int num2) {
		
		String str = "";
		
		
		for (int i = num1; i <= num2; i++) {
			double Asum = sumOfDigits(i);
			if (Asum == i)
				str = str + i + "\n";
		}
		return str;
	}

	public static double sumOfDigits(int num1) {
		
		int n = lengthOfNumber(num1);
		double sum = 0;
		int num = num1;
		
		for (int j = 1; j <= n; j++) {
			int digit = num % 10;
			sum = sum + Math.pow(digit, n);
			num = num / 10;
		}
		return sum;
	}

	public static int lengthOfNumber(int num) {
		
		int count = 0;
		
		while (num != 0) {
			num = num / 10;
			count++;
		}
		return count;

	}
}
