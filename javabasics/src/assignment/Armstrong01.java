package assignment;

public class Armstrong01 {

	public static void main(String[] args) {
		int num1 = 100;
		int num2 = 500;
		System.out.println(generateArmstrongNums(num1, num2));

	}

	public static String generateArmstrongNums(int start, int limit) {
		if (start <= 0 || limit <= 0)
			return "-1";
		if (start >= limit)
			return "-2";
		String result = "";
		for (int n = start; n <= limit; n++) {
			if (isArmstrong(n)) {
				result += n + ",";
			}
		}
		if (result.length() == 0) {
			return "-3";

		}
		result = result.substring(0, result.length() - 1);
		return result;
	}

	public static boolean isArmstrong(int num) {
		return (num == sumOfPowersOfDigits(num));
	}

	public static int sumOfPowersOfDigits(int n) {
		int digits[] = getDigits(n);
		int sum = 0;
		for (int digit : digits) {
			sum += power(digit, digits.length);
		}
		return sum;
	}

	public static int[] getDigits(int n) {

		String str = n + "";
		int[] digits = new int[str.length()];
		int index = 0;
		while (n != 0) {
			digits[index] = n % 10;
			index++;
			n /= 10;
		}
		return digits;

	}

	public static int power(int d, int p) {
		int pow = 1;
		for (int i = 1; i <= p; i++) {
			pow = pow * d;
		}
		return pow;
	}

}
