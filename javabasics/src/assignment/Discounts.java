package assignment;

import java.util.Scanner;

public class Discounts {

	public static void main(String[] args) {
		int price;

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the price : ");
		price = sc.nextInt();
		System.out.println("Selling price " + sellingPrice(price));

	}

	public static double sellingPrice(int cost) {
		double sellingPrice = 0.0;
		if (cost > 0 && cost <= 10000) {
			sellingPrice = cost - (cost / 10.0);
		} else if (cost > 10000 && cost <= 20000) {
			sellingPrice = cost - (cost / 5.0);
		} else if (cost > 20000) {
			sellingPrice = cost - (cost / 4.0);
		}
		return sellingPrice;
	}

}
