package assignment;

import java.util.Scanner;

public class Factors {

	public static void main(String[] args) {
		int num, sum = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		System.out.println(factorsOfNumber(num));
	}

	public static String factorsOfNumber(int num1) {
		int i = 1;
		String str = "";
		for (i = 1; i <= num1; i+=2) {
			if (num1 % i == 0)
				str = str + i + " ";

		}
		return str;
	}
}
