package assignment;

import java.util.Scanner;

public class FibonacciSeries {

	public static void main(String[] args) {

		int num1 = 0, num2 = 1, num3 = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		int num = sc.nextInt();
		System.out.println(printFibonacci(num, num1, num2, num3));
	}

	public static String printFibonacci(int num, int n1, int n2, int n3) {
		int i;
		n1 = 0;
		n2 = 1;

		String str = "";
		System.out.print(n1 + " " + n2);

		for (i = 2; i < num; ++i) {
			n3 = n1 + n2;
			n1 = n2;
			n2 = n3;
			str = str + ", " + n3;
		}
		return str;
	}
}
