package assignment;

public class FirstLastName {

	public static void main(String[] args) {
		
		String name = "Shivani Reddy";
		
		String firstname = name.substring(0, name.indexOf(" "));
		String lastname = name.substring(name.indexOf(" ")+1, name.length());
		
		System.out.println("FirstName : "  + firstname + "\n" + "LastName : " + lastname);

	}

}
