package assignment;

public class JulianDate01 {
	public static void main(String[] args) {
        String date = "29-Dec-2016";
        System.out.println(dateFormat(date));
 
    }
   
    public static String dateFormat(String date) {
    	String[] dateparts = date.split("-");
		
		int dd = Integer.parseInt(dateparts[0]);
		int mm = convertMMMtoMM(dateparts[1]);
		int year = Integer.parseInt(dateparts[2]);
		
		String julian = year + julianDate(dd, mm);
		
		return julian;
        
    }
   
   public static String julianDate(int dd, int mon) {
	   int[] months = {0,31,59,90,120,151,181,212,243,273,304,334};
	   
	   int jd = months[mon -1] + dd;
	   String day = "" + jd;
	   if(jd <= 9)
		   day = "00" + jd;
	   else if(jd <= 99)
		   day = "0" + jd;
	   
	   return day;
      
   }
   
   public static int convertMMMtoMM(String mon) {
	   String month = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
	   
		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();
		int monValue = (month.indexOf(mon) / 3) + 1;
		
		return monValue;
   }
}



