package assignment;

public class LargestPow2Collatz {

	public static void main(String[] args) {
		System.out.println("collatz sequence : " + getCollatzSequence(128));
		System.out.println("largest power of 2 in the Collatz sequence : " + largestPower(128));
	}

	public static int largestPower(int num1) {
		int power;

		if ((int) (Math.ceil((Math.log(num1) / Math.log(2)))) == (int) (Math.floor(((Math.log(num1) / Math.log(2)))))) {
			return num1;

		} else {
			String str = getCollatzSequence(num1);

			String[] stringArray = str.split(",");

			int len = stringArray.length;
			int i = len - 2;

			while (Integer.parseInt(stringArray[i]) % 2 == 0) {
				i--;
			}
			int num = Integer.parseInt(stringArray[i + 1]);
			

		}
		return num1;

	}

	

	public static String getCollatzSequence(int number) {
		String str = "";

		if (number <= 0)
			return "Error";

		while (number != 4) {

			str = str + number + ",";

			if (number % 2 != 0) {
				number = 3 * number + 1;
			}

			else {
				number = number / 2;
			}
		}

		str = str + number + ",2,1";
		return str;
	}

}
