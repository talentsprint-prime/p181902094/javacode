package assignment;

import java.util.Scanner;

public class Leapyear {

	public static void main(String[] args) {

		int year;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		year = sc.nextInt();

		if (year % 400 == 0) {
			System.out.println(year + " is a century leap year");
		} else if (year % 4 == 0 && year % 100 != 0) {
			System.out.println(year + " is a non century leap year");
		} else {
			System.out.println(year + " is not a leap year");
		}
	}
}
