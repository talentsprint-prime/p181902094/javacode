package assignment;

public class LuckyNumber01 {
	public static void main(String[] args) {
		String date = "2-jun-1997";
		System.out.println(getLuckyNumber(date));
	}

	public static int getLuckyNumber(String date) {
		String[] dateparts = date.split("-");
		int sum = 0;
		int dd = Integer.parseInt(dateparts[0]);
		int mm = convertMMMtoMM(dateparts[1]);
		int year = Integer.parseInt(dateparts[2]);

		sum += getSumOfDigits(dd);
		sum += getSumOfDigits(mm);
		sum += getSumOfDigits(year);

		while (sum >= 10) {
			sum = getSumOfDigits(sum);
		}
		return sum;

	}

	public static int convertMMMtoMM(String mon) {
		String month = "JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
		mon = mon.substring(0, 3);
		mon = mon.toUpperCase();
		int monValue = (month.indexOf(mon) / 3) + 1;

		return monValue;

	}

	public static int getSumOfDigits(int num) {
		int sum = 0;
		while (num > 0) {
			sum += num % 10;
			num = num / 10;
		}
		return sum;

	}
}
