package assignment;


import java.util.HashMap;
import java.util.Map;

public class MaxSpan {
	public static int findMaxFrequency(int[] arr) {
		int count = 1, tempCount;
		int popular = arr[0];
		int temp = 0;
		

		for (int i = 0; i < (arr.length - 1); i++) {
			if(arr[i] <= 0)
				return -1;
			
			temp = arr[i];
			tempCount = 0;
			for (int j = 1; j < arr.length; j++) {
				if (temp == arr[j])
					tempCount++;
			}
			if (tempCount > count) {
				popular = temp;
				count = tempCount;
			}
		}
		return popular;
	}

	public static void main(String[] args) {
		int[] arr = { 1, 1, 1, 1, 1, 1, 2, 2, 3, 3, 3, 3 };
		System.out.println(findMaxFrequency(arr));
	}
}