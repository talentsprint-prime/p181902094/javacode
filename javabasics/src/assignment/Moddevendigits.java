package assignment;

import java.util.Scanner;

public class Moddevendigits {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		System.out.println("even and odd digits in the given number respectively : "+ countEvenOddDigits( num));
	}
		
	public static String countEvenOddDigits(int num)
	{
		String str = "";
		int digit;
		int evennumbers = 0;
		int oddnumbers = 0;
		while (num > 0) {
			digit = num % 10;
			if(digit != 0)
			if (digit % 2 == 0)
				evennumbers++;
			else
				oddnumbers++;

			num = num / 10;
		}
		str = evennumbers + " " + oddnumbers;
		return str;
	}

}

