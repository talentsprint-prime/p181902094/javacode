package assignment;

import java.util.Scanner;

public class NextPalindrome {

	public static void main(String[] args) {
		int num;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number");
		num = sc.nextInt();
		System.out.println(nextPalindrome(num));
	}

	public static boolean isPalindrome(int num) {
		return (num == reverse(num));

	}

	public static int reverse(int num) {
		int rev = 0, digit;

		while (num > 0) {
			digit = num % 10;
			rev = rev * 10 + digit;
			num = num / 10;
		}
		return rev;
	}
	
	public static int nextPalindrome(int num){
		while(!isPalindrome(num)){
			num++;
		}
		return num;
	}
}
