package assignment;

import java.util.Scanner;

public class NumberOfWords {

	public static void main(String[] args) {
		String str;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the text");
		str = sc.nextLine();

		System.out.println("Number of  in the text : " + countWords(str));

	}

	public static int countWords(String str) {
		int count = 1;

		str = str.trim();
		for (int i = 0; i < str.length() - 1; i++) {
			if (((str.charAt(i) == ' ') && (str.charAt(i + 1) != ' ')) || (str.charAt(i) == ',')) {
				count++;

			}
		}
		return count;

	}

}
