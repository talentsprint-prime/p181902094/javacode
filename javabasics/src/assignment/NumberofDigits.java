package assignment;

import java.util.Scanner;

public class NumberofDigits {

	public static void main(String[] args) {
		int num;
		int digits = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		
		while(num != 0)
		{
			num = num/10;
			digits++;
		}
		if( digits == 5)
		System.out.println("given number is a five digit number");
		else
		System.out.println("given number is not a five digit number");
	
	}

}
