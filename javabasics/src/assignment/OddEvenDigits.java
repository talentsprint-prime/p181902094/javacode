package assignment;

import java.util.Scanner;

public class OddEvenDigits {

	public static void main(String[] args) {
		int num;
		int digit;
		int evennumbers = 0;
		int oddnumbers = 0;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();

		while (num > 0) {
			digit = num % 10;
			
			if (digit % 2 == 0)
				evennumbers++;
			else
				oddnumbers++;

			num = num / 10;
		}
		
		System.out.println("Number of even digits in the number : " + evennumbers);
		System.out.println("Number of odd digits in the number : " + oddnumbers);

	}

}
