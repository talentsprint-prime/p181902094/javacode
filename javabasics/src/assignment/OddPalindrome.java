package assignment;

import java.util.Scanner;

public class OddPalindrome {

	public static void main(String[] args) {
		int num1, num2;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the range");
		num1 = sc.nextInt();
		num2 = sc.nextInt();

		System.out.println(oddPalindrome(num1, num2));
	}

	public static int reverse(int number) {

		int rev = 0, digit;

		while (number > 0) {
			digit = number % 10;
			rev = rev * 10 + digit;
			number = number / 10;
		}
		return rev;
	}

	public static boolean isAllDigitsOdd(int num) {
		int digit;

		while (num > 0) {
			digit = num % 10;
			if (digit % 2 == 0) {
				return false;
			}
			num = num / 10;
		}
		return true;

	}

	public static String oddPalindrome(int num1, int num2) {
		int n;
		String str = "";

		for (n = num1; n <= num2; n++) {
			if (n == reverse(n)) {
				if (isAllDigitsOdd(n))
					str = str + n + "\n";
			}

		}
		return str;
	}

}
