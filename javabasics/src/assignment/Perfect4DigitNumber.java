package assignment;

public class Perfect4DigitNumber {

	public static void main(String[] args) {

		System.out.println(perfectSquaresRange(1000, 9999));

	}

	public static String perfectSquaresRange(int start, int end) {

		String str = "";

		for (int i = (int) Math.sqrt(start) + 1; i <= (int) Math.sqrt(end); i += 2) {
			int num = i * i;
			if (isAllDigitsEven(num)) {
				str = str + num + "\n";
			}
		}

		return str;
	}

	public static boolean isAllDigitsEven(int num) {

		int digit = 0;

		while (num > 0) {
			digit = num % 10;
			if (digit % 2 != 0) {
				return false;
			}
			num = num / 10;

		}

		return true;
	}

	/*public static boolean isperfectSquare(int num) {

		int square;

		square = (int) Math.sqrt(num);

		if (num == Math.pow(square, 2)) {
			return true;
		}
		return false;

	}*/

}
