package assignment;

import java.util.Scanner;

public class PerfectNumber {

	public static void main(String[] args) {
		int num;

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();

		if (isPerfectNumber(num))
			System.out.println(num + " is a perfect number");
		else
			System.out.println(num + " is not a perfect number");
	}

	public static int sumOfFactors(int num) {
		int sum = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0)
				sum = sum + i;
		}
		return sum;

	}

	public static boolean isPerfectNumber(int num) {

		return (sumOfFactors(num) == num);

	}

}
