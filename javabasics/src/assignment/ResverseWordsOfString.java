package assignment;

import java.util.*;

public class ResverseWordsOfString {

	public static void main(String[] args) {

		String str;
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter a string ");
		str = sc.nextLine();

		System.out.println(splitText(str));
	}

	public static String splitText(String str) {
		String str1 = "";
		
		String arr[] = str.split(" ");
		
		for (int i = 0; i < arr.length; i++) {
			str1 = str1 + (reversingWord(arr[i])) + " ";
		}
		return str1;
	}

	public static String reversingWord(String arr) {

		String reverse = "";
		int length = arr.length();

		for (int i = length - 1; i >= 0; i--) {
			reverse = reverse + arr.charAt(i);
		}
		return reverse.trim();
	}
}