package assignment;

import java.util.Scanner;

public class SerialNumbers {
	public static String getAllNumbers(int n) {
		String str = "";
		int [] a = new int[n];
		int j = 0;
		if(n<=0)
			str = str + (-1);
		for(int i =0;i < a.length;i +=2){
			if(i == 0)
				a[i] = i + 1;
			if(i == 2)
				a[i] = i;
			else if((i % 2 == 0) && (i >2))
				a[i] = i - i/2 + 1;
			
		}
		for (int i = 1;i < a.length; i += 2)
			a[i] = n - i/2;
		for(int number : a)
			str = str + number + " ";
		int x = str.length();
		str = str.substring(0, x - 1);
		return str;
	}

	public static void main(String[] args) {

		System.out.println(getAllNumbers(13));

	}
}
