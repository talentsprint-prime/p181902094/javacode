package assignment;

public class SixthStarPattern {
	public static String getStars(int n){
		String str;
		str = ""; 
        for (int i = 1; i < n; i++) {
            for (int j = 1; j <= i; j++){
            	str = str + "*";
            }
            str = str + "\n";
        }
        for (int i = n; i >= 0; i--) {
            for (int j = 1; j <= i; j++){
                str = str + "*";
            }
            if(i != 1)
            str = str + "\n";
        }
        return str;
    }

	public static void main(String[] args) {
		System.out.println(getStars(5));
		
	}
}


