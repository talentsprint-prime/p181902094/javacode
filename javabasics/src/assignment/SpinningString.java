package assignment;

public class SpinningString {
	public static void main(String[] arg) {
		System.out.println(SpinningString.rotate("talent", 1));
		System.out.println(SpinningString.rotate("talent", 2));
		System.out.println(SpinningString.rotate("talent", 3));

	}

	public static String rotate(String str, int no_of_positions) {
		String str1 = "";
		if (str == null ) {
			return null;
		}
		if(str == ""){
			return null;
		}
		char[] stringToCharArray = str.toCharArray();
		
		if (no_of_positions <= 0) {
			return str;
		}
		
		if (no_of_positions > str.length()) {
			return str;
		}
		
		for (int i = 0; i < no_of_positions; i++) {
			char c = stringToCharArray[str.length() - 1];
			for (int j = str.length() - 1; j >= 1; j--) {

				stringToCharArray[j] = stringToCharArray[j - 1];

			}
			stringToCharArray[0] = c;
		}

		for (int k = 0; k < stringToCharArray.length; k++) {
			str1 += stringToCharArray[k];
		}
		return str1;

	}

}
