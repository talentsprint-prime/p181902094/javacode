package assignment;

public class StrongNumber {
	public static void main(String[] args) {
		int num = 145;
		System.out.println(num + " is strong number : " + isStrongNumber(num));

	}

	public static boolean isStrongNumber(int num) {
		int sum = 0;
		int num1 = num;

		while (num1 != 0) {
			int digit = num1 % 10;

			sum += calculatefactorial(digit);
			num1 = num1 / 10;

		}
		if (sum == num)
			return true;
		else
			return false;
	}

	public static int calculatefactorial(int num) {
		int i;
		int factorial = 1;
		for (i = num; i > 1; i--) {
			factorial = factorial * i;
		}
		return factorial;
	}
}
