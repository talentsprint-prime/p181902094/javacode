package assignment;

import java.util.Scanner;

public class Timeconversion {

	public static void main(String[] args) {
		int minutes;
		int years;
		int days;
		int hours;
		int nmin;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the time in minutes :");
		minutes = sc.nextInt();
		
		years = minutes/(365*24*60);
		days = (minutes%(365*24*60))/(24*60);
		hours= (minutes%(365*24*60)-(days*24*60))/60;
		nmin = (minutes%(365*24*60)-(days*24*60)-(hours*60));
        
		System.out.println("Given minutes has " + years + " years " + days + " days " + hours + " hours and " + nmin + " minutes ");

	}

}
