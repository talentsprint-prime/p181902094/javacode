package assignment;

import java.util.Scanner;

public class Timeconversion2 {

	public static void main(String[] args) {
		int minutes;
		int seconds;
		int hours;
		int addsec;
		int totalsec;

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the time in hours :");
		hours = sc.nextInt();
		System.out.println("enter the time in minutes :");
		minutes = sc.nextInt();
		System.out.println("enter the time in seconds :");
		seconds = sc.nextInt();
		System.out.println("enter the seconds to be added :");
		addsec = sc.nextInt();

		totalsec = (hours * 60 * 60) + (minutes * 60) + seconds;
		totalsec = totalsec + addsec;
		hours = totalsec / 3600;
		minutes = (totalsec % 3600) / 60;
		seconds = (totalsec % 3600) % 60;
		System.out.println(hours + " hours " + minutes + " minutes " + seconds + " seconds");
	}
}
