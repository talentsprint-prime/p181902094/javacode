package assignment;

import java.util.Scanner;

public class ToCountVowelsConsonants {

	public static void main(String[] args) {

		String line;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the text");
		line = sc.nextLine();

		System.out.println(countCharacterType(line));
		System.out.println(nextAlphabet(line));
	}

	public static String countCharacterType(String line) {

		int vowels = 0, consonants = 0, digits = 0;

		line = line.toLowerCase();
		String str = "";

		for (int i = 0; i < line.length(); ++i) {
			char ch = line.charAt(i);
			if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u') {
				++vowels;
			} else if (Character.isAlphabetic(ch)) {
				++consonants;
			} else if (Character.isDigit(ch)) {
				++digits;
			}

		}
		str = str + "vowels : " + vowels + "\n" + "consonents : " + consonants + "\n" + "digits : " + digits;
		return str;
	}

	public static String nextAlphabet(String line) {

		String str = "";

		for (int i = 0; i < line.length(); ++i) {
			char ch = line.charAt(i);

			if ((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'z')) {
				ch = (char) (ch + 1);
				str += ch;
			} else
				str = str + line.charAt(i);

		}
		return str;

	}
}