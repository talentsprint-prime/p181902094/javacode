package assignment;

import java.util.Scanner;

public class palindrome {

	public static void main(String[] args) {
		int num;
		int digit;

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		if (palindrome(num))
			System.out.println("given number is palindrome");
		else
			System.out.println("given number is not palindrome");
	}

	public static boolean palindrome(int num) {
		int digit;
		int reversenumber = 0;
		int originalnumber = num;
		while (num > 0) {
			digit = num % 10;
			reversenumber = (reversenumber * 10) + digit;
			num = num / 10;
		}

		if (originalnumber == reversenumber)
			return true;
		else
			return false;
	}

}
