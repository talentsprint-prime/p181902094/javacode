package decisionmakingstatements;

import java.util.Scanner;

public class Attendance {

	public static void main(String[] args) {
		int nofClasses;
		int nofClassesAttended;
		double attendencePercentage;

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the total number of classes: ");
		nofClasses = sc.nextInt();
		System.out.println("enter the number of classes attended : ");
		nofClassesAttended = sc.nextInt();

		attendencePercentage = (nofClassesAttended / (double) nofClasses) * 100;
		System.out.println("Attendence percentage of the student is " + attendencePercentage);
		
		if (attendencePercentage < 75)
			System.out.println("Student is not allowed to sit for exam");
		else
			System.out.println("Student is allowed to sit for exam");

	}
}
