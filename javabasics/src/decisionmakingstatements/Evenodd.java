package decisionmakingstatements;

import java.util.Scanner;

public class Evenodd {

	public static void main(String[] args) {
		
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		
		if(num % 2 == 0) //single statement in if block so {} not necessary
			System.out.println(num + " is even number");

		else //single statement in else block so {} not necessary
			System.out.println(num + " is odd number");
		
	}

}
