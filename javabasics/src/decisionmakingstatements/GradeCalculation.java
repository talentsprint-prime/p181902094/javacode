package decisionmakingstatements;

import java.util.Scanner;

public class GradeCalculation {

	public static void main(String[] args) {
		int subject1;
		int subject2;
		int subject3;
		

		Scanner sc = new Scanner(System.in);
		System.out.println("enter the marks in subject1 : ");
		subject1 = sc.nextInt();
		System.out.println("enter the marks in subject2 : ");
		subject2 = sc.nextInt();
		System.out.println("enter the marks in subject3 : ");
		subject3 = sc.nextInt();

		System.out.println("Grade received = " + gradeCalculation(subject1,subject2,subject3));
	}
	
	public static char gradeCalculation(int sub1,int sub2,int sub3)
	{
	    double average = (sub1 + sub2 + sub3) / 3.0;
	    char ch;
		
		if (average >= 90)
			ch = 'A';
		else if (average >= 70)
			ch = 'B';
		else if (average >= 50)
			ch = 'C';
		else
			ch = 'F';
		return ch;
	}

}

	
