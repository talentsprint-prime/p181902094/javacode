package decisionmakingstatements;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		int num, i;
		int count = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number : ");
		num = sc.nextInt();

		for (i = 2; i <= num / 2; i++) {
			if (num % i == 0)
				count++;
		}
		if (count > 0)
			System.out.println(num + " is not prime");
		else
			System.out.println(num + " is prime");

	}

}
