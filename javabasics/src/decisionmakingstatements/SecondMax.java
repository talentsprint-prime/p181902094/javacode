package decisionmakingstatements;

import java.util.Scanner;

public class SecondMax {

	public static void main(String[] args) {
		int num;
		int input;
		int max = 0;
		int secondMax = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		while (num != 0) {

			input = num % 10;
			if (input > max) {
				secondMax = max;
				max = input;
			} else if ((input > secondMax)&&(max != input)) {
				secondMax = input;
			}
			num = num / 10;
		}
		System.out.println(secondMax);
		System.out.println(max);

	}

}
