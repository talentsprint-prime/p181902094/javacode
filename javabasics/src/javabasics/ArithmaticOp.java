package javabasics;

public class ArithmaticOp {

	public static void main(String[] args) {
		
		int num1 = 12;
		int num2 = 10;
		int add = num1 + num2;
		int sub = num1 - num2;
		int mul = num1 * num2;
		int div = num1 / num2;
		int mod = num1 % num2;
		
		System.out.println("The results of addition:" + add );
		System.out.println("The results of sub:" + sub );
		System.out.println("The results of multiplication:" + mul );
		System.out.println("The results of division:" + div );
		System.out.println("The results of modulus:" + mod );
		
		System.out.println( add +"\n" + sub + "\n" + mul + "\n" + div + "\n" + mod);
				

	}

}
