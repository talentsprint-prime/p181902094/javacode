package javabasics;

public class Circle {

	public static void main(String[] args) {
		
	    float pi=3.14f;
		float radius = 5;
		float area = pi * radius * radius;
		float perimeter = 2 * pi* radius;
		
		System.out.println("area of circle is "+ area);
        System.out.println("perimeter of circle is " + perimeter);
       
	}

}
