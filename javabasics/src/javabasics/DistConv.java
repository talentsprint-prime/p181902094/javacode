package javabasics;

import java.util.Scanner;

public class DistConv {

	public static void main(String[] args) {
		float dist;
		int hours;
		int min;
		int sec;
		int totalsec;
		double meterPsec;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the distance : ");
		dist = sc.nextFloat();
		System.out.println("enter hours : ");
		hours = sc.nextInt();
		System.out.println("enter minutes : ");
		min = sc.nextInt();
		System.out.println("enter seconds : ");
		sec = sc.nextInt();
		totalsec = (hours*3600)+(min*60)+sec;
		meterPsec = totalsec/dist;
		System.out.println("speed in meters per second = " + meterPsec);
		System.out.println("speed in kilometer per hour = " + (meterPsec*3.6));
		System.out.println("speed in mile per hour  = " + (meterPsec*2.237));

		
	}

}
