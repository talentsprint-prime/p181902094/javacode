package javabasics;

import java.util.Scanner;

public class Swap {

	public static void main(String[] args) {
		int num1;
		int num2;
		int temp;
		
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number1 : ");
		num1 = sc.nextInt();
		System.out.println("enter the number2 : ");
		num2 = sc.nextInt();
		
		temp = num1;
		num1 = num2;
		num2 = temp;
		
		System.out.println(" number1 : "+num1);
		System.out.println(" number2 : "+num2);
		
		

	}

}
