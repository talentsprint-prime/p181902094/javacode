package loops;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		int num;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the number : ");
		num = sc.nextInt();
		System.out.println("Factorial of given number is " + calculatefactorial(num));
	}
	public static int calculatefactorial(int num){
		int i;
		int factorial = 1;
		for (i = num; i > 1; i--) {
			factorial = factorial * i;
		}
		return factorial;
	}

}
