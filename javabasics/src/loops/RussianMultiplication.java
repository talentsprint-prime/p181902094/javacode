package loops;

public class RussianMultiplication {

	public static void main(String[] args) {
		
		int num1 = 27, num2 = 35;
		System.out.println(getProduct(num1,num2));
		
	}
	
	public static String getProduct(int num1,int num2){
		String str = "";
		int sum1 = 0;
		
		while(num1 >= 1){
			 if(num1 % 2 != 0)
			 {
	        	sum1 = sum1 + num2;	
	        	str  = str + num2 + " + ";
			  }
			    num1 = num1/2;
				num2 = num2*2;
		}
		str = str.substring(0, str.length() -2);
		str = str + "= " + sum1;
		return str;
		
		
	}

}
