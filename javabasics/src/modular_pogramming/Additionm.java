package modular_pogramming;

public class Additionm {

	public static void main(String[] args) {
      int num1 = 15;
      int num2 = 2;
      System.out.println("sum is " + calculateSum(num1,num2));
      System.out.println("Difference is " + calculateDiff(num1,num2));
      System.out.println("Product is " + calculateMul(num1,num2));
      System.out.println("Division is " + calculateDiv(num1,num2));

	}
	
	public static int calculateSum(int n1,int n2) {
		int sum = n1 + n2;
		return sum;
	}
	public static int calculateDiff(int n1,int n2) {
		int diff = n1 - n2;
		return diff;
	}
	public static int calculateMul(int n1,int n2) {
			int mul = n1 * n2;
			return mul;
	}
	public static double calculateDiv(int n1,int n2) {
				double div = (double)n1/ (double)n2;
				return div;
	}
}

