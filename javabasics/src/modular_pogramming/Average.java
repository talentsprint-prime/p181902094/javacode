package modular_pogramming;

public class Average {

	public static void main(String[] args) {
		int num1 = 10;
		int num2 = 22;
		int num3 = 30;
		System.out.println(calculateAverage(num1, num2, num3));
	}

	public static int calculateSum(int num1, int num2, int num3) {
		int sum = (num1 + num2 + num3);
		return sum;
	}

	public static double calculateAverage(int num1, int num2, int num3) {
		double average = calculateSum(num1, num2, num3) /3.0;
		return average;
	}

}
