package modular_pogramming;

public class Circle {

	public static void main(String[] args) {
		int radius = 5;
		System.out.println("area of the circle is : " + area(radius));
		System.out.println("perimeter of the circle is : " + perimeter(radius));

	}

	public static double area(int radius) {
		double area = Math.PI* radius * radius;
		return area;
	}

	public static double perimeter(int radius) {
		double perimeter = 2 * Math.PI* radius;
		return perimeter;
	}
}
