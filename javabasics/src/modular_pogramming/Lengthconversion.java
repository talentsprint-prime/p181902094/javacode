package modular_pogramming;

import java.util.Scanner;

public class Lengthconversion {

	public static void main(String[] args) {
		double length;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the length in inches : ");
		length = sc.nextDouble();

		System.out.println("length in meters : " + inchToMeter(length));

	}

	public static double inchToMeter(double length) {
		double meters = length / 39.37;
		return meters;
	}

}
