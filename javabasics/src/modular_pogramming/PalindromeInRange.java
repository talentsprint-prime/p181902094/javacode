package modular_pogramming;

import java.util.Scanner;

public class PalindromeInRange {
	public static void main(String[] args) {
		
		int num1, num2;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the range");
		num1 = sc.nextInt();
		num2 = sc.nextInt();

		System.out.println(palindromeInRange(num1, num2));
	}

	public static String palindromeInRange(int num1, int num2) {
		
		int n;
		String str = "";

		for (n = num1; n <= num2; n++) {
			if (n == reverse(n)) 
				str = str + n + "\n";
		}
		return str;
	}

	public static int reverse(int number) {

		int rev = 0, digit;

		while (number > 0) {
			digit = number % 10;
			rev = rev * 10 + digit;
			number = number / 10;
		}
		return rev;
	}
}
