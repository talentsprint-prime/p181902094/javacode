package modular_pogramming;

public class SumofDigits {

	public static void main(String[] args) {
		int num = 23;

		System.out.println("Sum of the digits in the given number :" + calculateSumofDigits(num));

	}

	public static int calculateSumofDigits(int num1) {
		if(num1 > 0 && num1 < 100){
		int sum = (num1 % 10) + (num1 / 10);
		return sum;
		}else
			return 0;

	}

}
